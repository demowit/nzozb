﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Controllers
{
    public partial class HomeController : Controller
    {
        public ActionResult GETClinicDescription(int clinicId)
        {
            ShortClinicInformation clinicInformation = null;
            using (var db = new ZOZB())
            {
                clinicInformation = db.Clinic.Where(a => a.Id == clinicId).
                    Select(a => new ShortClinicInformation()
                    {
                        Id = a.Id,
                        ClinicDescription = a.ClinicDescription,
                        ClinicName = a.ClinicName
                    }).
                    First();
            }
            return PartialView("_ClinicDescription", clinicInformation);
        }

        private List<ShortClinicInformation> GetClinicList(int healthCenterId)
        {
            List<ShortClinicInformation> clinics = null;
            using (var db = new ZOZB())
            {
                clinics = db.HealthCenterClinic.Where(a => a.HealthCenterId == healthCenterId).
                    Select(a => new ShortClinicInformation()
                    {
                        Id = a.ClinicId,
                        ClinicName = a.Clinic.ClinicName,
                        ClinicDescription = a.Clinic.ClinicDescription,
                    }).ToList();
            }
            return clinics;
        }
        public ActionResult GETClinicList(int healthCenterId)
        {
            return PartialView("_ClinicList", GetClinicList(healthCenterId));
        }

        public ActionResult Clinic()
        {
            ClinicInformation clinics = new ClinicInformation();
            using (var db = new ZOZB())
            {
                clinics.Centers = db.HealthCenter.Select(a => new ShortCenterInformation()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName,
                }).ToList();
                clinics.Clinics = (clinics.Centers.Count > 0) ? GetClinicList(clinics.Centers[0].CenterId) : clinics.Clinics;
            }
            return View("Clinic", clinics);
        }
    }
}