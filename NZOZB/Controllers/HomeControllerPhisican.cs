﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Controllers
{
    public partial class HomeController : Controller
    {
        private FullPhisicanInformation GetFullPhisicanInformation(int phisicanId)
        {
            FullPhisicanInformation result = null;
            using (var db = new ZOZB())
            {
                var shortInformation = db.Phisican.Where(a => a.Id == phisicanId).
                    Select(a => new
                    {
                        a.FirstName,
                        a.LastName,
                        PhisicanId = a.Id,
                        a.PhotoLink,
                    }).First();

                if (shortInformation != null)
                {
                    var fullInformation = db.PhisicanOpinion.Where(a => a.PhisicanId == phisicanId);
                    result = new FullPhisicanInformation()
                    {
                        Opinion = fullInformation.ToList(),
                        AverrageRate = fullInformation.Average(a => a.Rate),
                        FirstName = shortInformation.FirstName,
                        LastName = shortInformation.LastName,
                        PhisicanId = shortInformation.PhisicanId,
                        PhotoName = shortInformation.PhotoLink,
                    };
                }
            }
            return result;
        }

        public ActionResult GETFullPhisicanInformation(int phisicanId)
        {
            return PartialView("_PhisicanFullInformation", GetFullPhisicanInformation(phisicanId));
        }

        private Dictionary<string, List<ShortPhisicanInformation>> GetPhisicanList(int healthCenterId)
        {
            Dictionary<string, List<ShortPhisicanInformation>> phisicans = null;
            using (var db = new ZOZB())
            {
                phisicans = db.PhisicanSpecialization.Join(db.PhisicanSpecializationName, a => a.SpecializationNameId, b => b.Id, (a, b) => new
                {
                    a.PhisicanId,
                    a.NotFinished,
                    a.SpecializationNameId,
                    b.SpecializationName
                }).Join(db.Phisican, a => a.PhisicanId, b => b.Id, (a, b) => new
                {
                    a.NotFinished,
                    a.PhisicanId,
                    a.SpecializationName,
                    a.SpecializationNameId,
                    b.FirstName,
                    b.LastName
                }).Join(db.PhisicanHealthCenter, a => a.PhisicanId, a => a.PhisicanId, (a, b) => new
                {
                    a.PhisicanId,
                    a.FirstName,
                    a.LastName,
                    a.SpecializationNameId,
                    a.SpecializationName,
                    a.NotFinished,
                    b.HealthCenterId
                }).Where(a => a.HealthCenterId == healthCenterId).
                GroupBy(a => a.SpecializationName, a => new ShortPhisicanInformation()
                {
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    PhisicanId = a.PhisicanId,
                    SpecializationName = a.SpecializationName,
                    SpecializationNameId = a.SpecializationNameId
                }).ToDictionary(a => a.Key, a => a.ToList());
            }
            return phisicans;
        }
        
        public ActionResult PhisicanHealthCenter(int healthCenterId)
        {
            List<ShortPhisicanInformation> phisican = new List<ShortPhisicanInformation>();
            using (var db = new ZOZB())
            {
                phisican = db.PhisicanHealthCenter.Join(db.Phisican, a => a.PhisicanId, b => b.Id, (a, b) => new 
                {
                    a.HealthCenterId,
                    b.FirstName,
                    b.LastName,
                    b.Id,
                }).Where(a => a.HealthCenterId == healthCenterId).Select(a => new ShortPhisicanInformation()
                {
                    PhisicanId = a.Id,
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                }).ToList();
            }
            return Json(phisican, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PhisicanCalendar(int healthCenterId, int phisicanId, DateTime visitDate)
        {
            return View();
        }
        public ActionResult PhisicanSpecialization(int healthCenterId, int specializationId)
        {
            List<ShortPhisicanInformation> phisican = new List<ShortPhisicanInformation>();
            using (var db = new ZOZB())
            {
                phisican = db.PhisicanHealthCenter.Join(db.Phisican, a => a.PhisicanId, b => b.Id, (a, b) => new
                {
                    a.PhisicanId,
                    a.HealthCenterId,
                    b.FirstName,
                    b.LastName,
                }).Join(db.PhisicanSpecialization, a => a.PhisicanId, b => b.PhisicanId, (a, b) => new
                {
                    a.PhisicanId,
                    a.FirstName,
                    a.LastName,
                    a.HealthCenterId,
                    b.SpecializationNameId,
                }).Where(a => a.SpecializationNameId == specializationId && a.HealthCenterId == healthCenterId).
                Select(a => new ShortPhisicanInformation()
                {
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    PhisicanId = a.PhisicanId,
                    SpecializationNameId = a.SpecializationNameId
                }).ToList();
            }
            return Json(phisican, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GETPhisicanList(int healthCenterId)
        {
            return PartialView("_PhisicanList", GetPhisicanList(healthCenterId));
        }
        public ActionResult Phisican()
        {
            PhisicanInformation phisicans = new PhisicanInformation();
            using (var db = new ZOZB())
            {
                phisicans.Centers = db.HealthCenter.Select(a => new ShortCenterInformation()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName
                }).ToList();
                phisicans.Phisicans = (phisicans.Centers.Count > 0) ? GetPhisicanList(phisicans.Centers[0].CenterId) : phisicans.Phisicans;
                int? phisicanId = (phisicans.Phisicans.Count > 0) && (phisicans.Phisicans.ElementAt(0).Value.Count > 0) ? phisicans.Phisicans.ElementAt(0).Value[0].PhisicanId : null;
                phisicans.FullPhisicanInformation = (phisicanId != null) ? GetFullPhisicanInformation((int)phisicanId) : null;
            }
            return View("Phisican", phisicans);
        }
    }
}