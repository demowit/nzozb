﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Controllers
{
    public partial class HomeController : Controller
    {

        public ActionResult GETShortCenterInformation(int healthCenterId)
        {
            ShortCenterInformation shortCenterInformation = null;
            using (var db = new ZOZB())
            {
                shortCenterInformation = db.HealthCenter.Where(a => a.Id == healthCenterId).Select(a => new ShortCenterInformation()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName,
                    Description = a.CenterDescription,
                    Street = a.Street,
                    PostCode = a.PostCode,
                    City = a.City
                }).First();
            }
            return PartialView("_IndexCenterInformation", shortCenterInformation);
        }

        public ActionResult Index()
        {
            List<ShortCenterInformation> list = null;
            using (var db = new ZOZB())
            {
                list = db.HealthCenter.Select(a => new ShortCenterInformation()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName,
                    Description = a.CenterDescription,
                    Street = a.Street,
                    PostCode = a.PostCode,
                    City = a.City
                }).ToList();
            }
            return View("Index", list);
        }
    }
}