﻿//#define Test
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Controllers
{
    public partial class HomeController : Controller
    {
#if Test
        private static string firstName = "Jarosław";
        private static string lastName = "Wiechecki";
        private static string motherName = "Sowa";
        private static DateTime birthDate = new DateTime(1970, 6, 27);
        private static string pesel = "70062701335";
        private static string city = "Bydgoszcz";
        private static string insuranceCardNumber = "100200300";
        private static string mailAddress = "jarek@wiechecki.org";
        private static string phisicanName = "Jan Kowalski";
        private static string phoneNumber = "700800900";
        private static string postCode = "85-791";
        private static Sex sex = Sex.male;
        private static string street = "Osiedlowa 4/22";
        private static List<ShortPhisicanInformation> phisican;
        private static List<ShortCenterInformation> center;
        private PatientAccount TestUserData(PatientData patientData)
        {
            firstName = patientData.FirstName;
            lastName = patientData.LastName;
            motherName = patientData.MotherName;
            pesel = patientData.Pesel;
            street = patientData.Street;
            city = patientData.City;
            postCode = patientData.PostCode;
            PatientAccount patientAccount = new PatientAccount()
            {
                PatientData = GetTestPatientData()
            };
            return patientAccount;
        }
        private PatientData GetTestPatientData()
        {
            center = new List<ShortCenterInformation>()
            {
                new ShortCenterInformation()
                {
                    CenterId = 1,
                    CenterName = "Centrum pierwsze",
                },
                new ShortCenterInformation()
                {
                    CenterId = 2,
                    CenterName = "Centrum drugie",
                },
                new ShortCenterInformation()
                {
                    CenterId = 3,
                    CenterName = "Centrum trzecie",
                }
            };
            phisican = new List<ShortPhisicanInformation>()
            {
                new ShortPhisicanInformation()
                {
                    FirstName = "Lekarz",
                    LastName = "One",
                    PhisicanId = 1,
                },
                new ShortPhisicanInformation()
                {
                    FirstName = "Lekarz",
                    LastName = "Two",
                    PhisicanId = 2,
                },
                new ShortPhisicanInformation()
                {
                    FirstName = "Lekarz",
                    LastName = "Three",
                    PhisicanId = 1,
                },
            };
            return new PatientData()
            {
                FirstName = firstName,
                LastName = lastName,
                MotherName = motherName,
                BirthDate = birthDate,
                Pesel = pesel,
                City = city,
                InsuranceCardNumber = insuranceCardNumber,
                MailAddress = mailAddress,
                PhisicanName = phisicanName,
                PhoneNumber = phoneNumber,
                PostCode = postCode,
                Sex = sex,
                Street = street,
                HealthCenter = center,
                Phisican = phisican,
                HealthCenterId = 1,
                PhisicanId = 1,
            };
        }
#endif
        private PatientAccount SaveUserData(PatientData patientData)
        {
            PatientAccount patientAccount = new PatientAccount();
            using (var db = new ZOZB())
            {
                Patient patient = new Patient()
                {
                    BirthDate = patientData.BirthDate,
                    City = patientData.City,
                    FirstName = patientData.FirstName,
                    HealthCenterId = patientData.HealthCenterId,
                    InsuranceCardNumber = patientData.InsuranceCardNumber,
                    LastName = patientData.LastName,
                    MailAddress = patientData.MailAddress,
                    MotherFamilyName = patientData.MotherName,
                    NFZRegionCodeId = patientData.NFZRegionCodeId,
                    Pesel = patientData.Pesel,
                    PhisicanId = patientData.PhisicanId,
                    PhoneNumber = patientData.PhoneNumber,
                    PostCode = patientData.PostCode,
                    Street = patientData.Street,
                    Sex = Convert.ToInt32(patientData.Pesel.Substring(7, 4)) % 2,
                };
                db.Patient.Add(patient);
                db.SaveChanges();               
                patientAccount.PatientData = GetPatientData(patientData.MailAddress);
            }
            return patientAccount;
        }
        [HttpPost]
        [Authorize]
        public ActionResult UserData(PatientData patientData)
        {
#if Test
            return View("YourAccount", TestUserData(patientData));
#else
            return View("YourAccount", SaveUserData(patientData));
#endif
        }
        private PatientData GetPatientData(Func<Patient, bool> predicate)
        {
            PatientData patientData = null;
            using (var db = new ZOZB())
            {
                patientData = db.Patient.Where(predicate).Select(a => new PatientData()
                {
                    BirthDate = a.BirthDate,
                    FirstName = a.FirstName,
                    City = a.City,
                    HealthCenter = db.HealthCenter.Select(b => new ShortCenterInformation()
                    {
                        CenterId = b.Id,
                        CenterName = b.CenterName,
                        City = a.City,
                        Description = b.CenterDescription,
                        PostCode = a.PostCode,
                        Street = a.Street,
                    }).ToList(),
                    HealthCenterId = a.HealthCenterId,
                    LastName = a.LastName,
                    InsuranceCardNumber = a.InsuranceCardNumber,
                    MailAddress = a.MailAddress,
                    MotherName = a.MotherFamilyName,
                    Pesel = a.Pesel,
                    Phisican = db.Phisican.Select(b => new ShortPhisicanInformation()
                    {
                        FirstName = b.FirstName,
                        LastName = b.LastName,
                        PhisicanId = b.Id,
                        PhotoName = b.PhotoLink,
                        SpecializationName = null,
                        SpecializationNameId = null,
                    }).ToList(),
                    PhisicanId = a.PhisicanId,
                    PhisicanName = db.Phisican.Where(b => b.Id == a.PhisicanId).Select(b => new
                    {
                        FullName = b.FirstName + " " + b.LastName
                    }).First().FullName,
                    PhoneNumber = a.PhoneNumber,
                    PostCode = a.PostCode,
                    Sex = (Sex)a.Sex,
                    Street = a.Street,
                    NFZRegionCodeId = a.NFZRegionCodeId,
                    NFZRegionalCode = db.NFZRegionCode.ToList(),
                }).First();
            }
            return patientData;
        }
        private PatientData GetPatientData(string userName)
        {
            return GetPatientData(a => a.MailAddress.Equals(userName, StringComparison.CurrentCultureIgnoreCase));
        }
        private PatientData GetPatientData(int patientId)
        {
#if Test
            return = GetTestPatientData();
#else
            return GetPatientData(a => a.Id == patientId);
#endif
        }

        [Authorize]
        public ActionResult GETPatientData()
        {
            return PartialView("_PatientData", GetPatientData(User.Identity.Name));
        }
        [Authorize]
        public ActionResult GETPatientVisit()
        {
            Calendar calendar = new Calendar();
            using (var db = new ZOZB())
            {
                calendar.Center = db.HealthCenter.ToList();
                calendar.Phisican = new List<Phisican>();
                calendar.Specialization = db.PhisicanSpecializationName.ToList();

            }
            return PartialView("_PatientVisit", calendar);
        }
        [Authorize]
        public ActionResult GETPatientMedicine()
        {
            return PartialView("_PatientMedicine");
        }
        [Authorize]
        public ActionResult YourAccount()
        {
            PatientAccount patientAccount = new PatientAccount()
            {
                PatientData = GetPatientData(User.Identity.Name)
            };
            return View("YourAccount", patientAccount);
        }
        [Authorize]
        public ActionResult PatientVisit()
        {
            Calendar calendar = new Calendar();
            using (var db = new ZOZB())
            {
                calendar.Center = db.HealthCenter.ToList();
                calendar.Phisican = new List<Phisican>();
                calendar.Specialization = db.PhisicanSpecializationName.ToList();

            }
            return PartialView("_PatientVisit", calendar);
        }
    }
}