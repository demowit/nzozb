﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Controllers
{
    public partial class HomeController : Controller
    {
        private HealthCenterContactCard GetHealthCenterCard(int healthCenterId)
        {
            HealthCenterContactCard card = null;
         

            using (var db = new ZOZB())
            {

                card = db.HealthCenter.Where(a => a.Id == healthCenterId).Select(a => new HealthCenterContactCard()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName,
                    City = a.City,
                    Street = a.Street,
                    PostCode = a.PostCode,
                    Mail = a.HealthCenterMail.ToList(),
                    Phone = a.HealthCenterPhone.ToList(),
                    OpeningHour = a.OpeningHour.ToList(),
                }).First();
            }
            return card;
        }

        public ActionResult GETHealthCenterCard(int healthCenterId)
        {
            return PartialView("_ContactCenterAddress", GetHealthCenterCard(healthCenterId));
        }

        public ActionResult Contact()
        {
            ContactCards contactCards = new ContactCards();
            using (var db = new ZOZB())
            {
                var query = db.HealthCenter.Select(a => new ShortCenterInformation()
                {
                    CenterId = a.Id,
                    CenterName = a.CenterName,
                });
                contactCards.Centers = query.ToList();
            }
            contactCards.ContactCard = (contactCards.Centers.Count == 0) ? null : GetHealthCenterCard(contactCards.Centers[0].CenterId);
            return View("Contact", contactCards);
        }
    }
}