﻿var commonApi = (function () {
    $('.date-picker').datepicker();
    var test = 0;
    return {
        setTestClick: function () {
            $('#patient-visit-test-button').click(function () {
                var centerId = $('#patient-visit-health-center').val();
                var phisicanId = $('#patient-visit-phisican').val();
                var visitDate = $('#visit-date').val();
                if ((centerId === '') || (phisicanId === '') || (visitDate === '')) return;
                $('#patient-visit-test-paragraph').html(centerId + ' ' + phisicanId + ' ' + visitDate);
            });
        },
        setDatePicker: function (datePickerId) {
            $(datePickerId).datepicker();
        },
        setDateChangeEvent: function (datePickerId) {
            //$(datePickerId).datepicker().on('change', function(ev){
            //    $('#patient-visit-test').val(test);
            $(datePickerId).on('dp.change', function (ev) {
                $('#patient-visit-test').val(test);
                test++;
            });
        },
        getPhisicanNamesBySpecialization: function (healthCenterSourceName, phisicanSpecializationSourceName, phisicanNameTargetName) {
            var centerId = $(healthCenterSourceName).val();
            var phisicanSpecializationId = $(phisicanSpecializationSourceName).val();
            if ((centerId === '') || (centerId === undefined) || (phisicanSpecializationId === '') || (phisicanSpecializationId === undefined)) {
                $(phisicanNameTargetName).html('');
                $(phisicanNameTargetName).append($('<option></option>').html('Wybierz lekarza'));
            }
            $.ajax({
                url: '/Home/PhisicanSpecialization',
                type: 'GET',
                dataType: 'JSON',
                data: { healthCenterId: centerId, specializationId: phisicanSpecializationId },
                success: function (phisicans) {
                    $(phisicanNameTargetName).html('');
                    $(phisicanNameTargetName).append($('<option></option>').html('Wybierz lekarza'));
                    $.each(phisicans, function (id, phisican) {
                        $(phisicanNameTargetName).append($('<option></option>').val(phisican.PhisicanId).html(phisican.FullName));
                    });
                }
            })
        },
        getPhisicanNames: function (healthCenterSourceName, phisicanNameTargetName) {
            var centerId = $(healthCenterSourceName).val();
            if ((centerId === '') || (centerId === undefined)) {
                $(phisicanNameTargetName).html('');
                $(phisicanNameTargetName).append($('<option></option>').html('Wybierz lekarza'));
            }
            $.ajax({
                url: '/Home/PhisicanHealthCenter',
                type: 'GET',
                dataType: 'JSON',
                data: { healthCenterId: centerId },
                success: function (phisicans) {
                    $(phisicanNameTargetName).html('');
                    $(phisicanNameTargetName).append($('<option></option>').html('Wybierz lekarza'));
                    $.each(phisicans, function (id, phisican) {
                        $(phisicanNameTargetName).append(
                            $('<option></option>').val(phisican.PhisicanId).html(phisican.FullName));
                    });
                }
            });
        },
    }
})();

