﻿--Terminologia:
--  Przychodnia HealthCenter
--  Poradnia Clinic
--  Lekarz Phisican
--  Pacjent Patient
--  Kalendarz Calendar
--  Otwarcie Opening
--  Rejestracja Registering
--  Specializacja Specialization
--  Specializacja kliniki ClinicSpecialization
--  Specialiacja lekarza PhisicanSpecialization
--  Lekarstwa Medicine
--  Recepty Prescription
--
-- Struktura Firmy:
--  
--  Przychodnia 1
--    Poradnia 11
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 12
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 1N
--      Lekarz 
--      Lekarz 
--      Lekarz 
--  
--  Przychodnia 2
--    Poradnia 21
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 22
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 2N
--      Lekarz 
--      Lekarz 
--      Lekarz 
--  
--  Przychodnia N
--    Poradnia N1
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia N2
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia NN
--      Lekarz 
--      Lekarz 
--      Lekarz 
--
-- Tabele podstawowe:

-- tabela zawiera nazwy i adresy przychodni
create type ObjectFirstName from nvarchar(50)
create type ObjectName from nvarchar(100)
create type ObjectDescription from nvarchar(max)

create table zozb.dbo.HealthCenter(
	Id int not null identity(1, 1),
	CenterName nvarchar(100),
	CenterDescription nvarchar(max),
	Street nvarchar(50),
	PostCode nvarchar(6),
	City nvarchar(50),
	constraint PKHealthCenterId primary key (Id)
	);

  
-- tabela zawiera nazwy poradni ich lokalizację w przychodni
create table zozb.dbo.Clinic(
    Id int not null identity(1, 1),
    ClinicName nvarchar(50),
    ClinicDescription nvarchar(max),
    constraint PKClinicId primary key (Id)
    );


  
-- tabele asocjacyjne dla Przychodni i Poradni
-- Przychodnia - Poradnia Jedna przychodnia - wiele poradni. Jedna poradnia - jedna przychodnia

create table zozb.dbo.HealthCenterClinic(
    Id int not null identity(1, 1),
    HealthCenterId int,
    ClinicId int,
    constraint PKHealthCenterClinicId primary key (Id)
    );

  
-- tabela z numerami telefonów
create table zozb.dbo.HealthCenterPhone(
    Id int not null identity(1, 1),
    HealthCenterId int, -- id przychodni lub poradni
    Number nvarchar(20),
    NumberDescription nvarchar(100),
    constraint PKHealthCenterPhoneId primary key (Id)
    );

-- tabela z numerami maili przychodni
create table zozb.dbo.HealthCenterMail(
    Id int not null identity(1, 1),
    HealthCenterId int, -- id przychodni lub poradni
    Address nvarchar(320),
    AddressDescription nvarchar(100),
    constraint PKHealthCenterMailId primary key (Id)
    );

-- tabela zawierająca godziny otwarcia przychodni
create table zozb.dbo.OpeningHour(
    Id int not null identity(1, 1),
    HealthCenterId int, -- id przychodni lub poradni
    StartTime time,
    FinishTime time,
    DayNumber int, -- numer dnia (pn, wt, itp) 
    constraint PKOpeningHourId primary key (Id)
    );

-- nazwiska lekarzy
create table zozb.dbo.Phisican(
	Id int not null identity(1,1),
	FirstName nvarchar(30),
	LastName nvarchar(50),
	PhotoLink nvarchar(2000) default null,
	VisitTimeLength numeric(5, 0),
	constraint PH_Phisican primary key (Id)
	);

-- nazwy specjalizacji lekarskich
create table zozb.dbo.PhisicanSpecializationName(
	Id int not null identity(1,1),
	SpecializationName nvarchar(50),
	Description nvarchar(max),
	constraint PK_PhisicanSpecializationName primary key (Id)
	);

-- specjalizacja lekarza. Jeden lekarz - wiele specjalizacji
create table zozb.dbo.PhisicanSpecialization(
	Id int not null identity (1,1),
	PhisicanId int,
	SpecializationNameId int,
	NotFinished tinyint default 0, 
	constraint PK_PhisicanSpecialization primary key (Id),
	);


-- opinie o lekarzach
create table zozb.dbo.PhisicanOpinion(
	Id int not null identity(1,1),
	PhisicanId int,
	Opinion nvarchar(max),
	Rate int,
	OpinionDate date
	constraint PK_PhisicanOpinionId primary key (Id),
	);

create table zozb.dbo.PhisicanHealthCenter(
	Id int not null identity(1, 1),
	PhisicanId int,
	HealthCenterId int,
	constraint PK_PhisicanHealthCenterId primary key clustered (Id)
	);


-- Tabela z pacjentami.
-- Na podstawie danych z tabeli generowany jest dokument przystąpinia do przychodni
-- może zostać on wydrukowany lub zapisany jako pdf na dysku

create table zozb.dbo.Patient(
	Id int not null identity(1, 1),
	FirstName nvarchar(50) not null,
	LastName nvarchar(100) not null,
	MotherFamilyName nvarchar(100) not null,
	BirthDate Date not null,
	Sex int not null, -- 0 male 1 female
	Pesel nvarchar(11),
	Street nvarchar(100),
	City nvarchar(100),
	PostCode nvarchar(6),
	PhoneNumber nvarchar(20),
	MailAddress nvarchar(320), 
	InsuranceCardNumber nvarchar(30), -- może być pusta null
	NFZCodeId int not null,
	RegisterDate Date not null,
	PhisicanId int, -- kod lekarza podstawowej opieki lekarskiej. może być pusty (null)
	HealthCenterId int, -- kod przychodni do której zapisany jest pacjent
	constraint PKPatientId primary key (Id)
	);

create table zozb.dbo.NFZRegionCode(
    Id int not null identity(1, 1),
    NFZCode nvarchar(2) not null,
    Region nvarchar(250),
    constraint PKNFZCodeId primary key (Id)
    );


-- tabele dotyczące lekarstw
-- rodzaje odpłatności za leki
create table zozb.dbo.PaymentKind(
    Id int not null identity(1, 1),
    PayementName nvarchar(30),
    constraint PKPaymentKindId primary key (Id)
    );

-- tabela z kategoriami lekarstw
create table zozb.dbo.MedicineCategory(
    Id int not null identity(1, 1),
    CategoryName nvarchar(100),
    constraint PKMedicineCategoryId primary key (Id)
    );

-- tabela z lekarstwami. spełnia tę samą rolę co tabela z produktami w sklepie.
create table zozb.dbo.Medicine(
    Id int not null identity(1, 1),
    CategoryId int,
    MedicineName nvarchar(100),
    MedicineDescription nvarchar(max),
    constraint PKMedicineId primary key (Id)
    );

-- tabele asocjacyjne.
-- tabela leków przepisanych pacjentowi przez lekarza. Na jej podstawie pacjent generuje zamówienie na receptę.
create table zozb.dbo.PatientMedicine(
    Id int not null identity(1, 1),
    PatientId int,
    MedicineId int,
    Dosage nvarchar(100),
    PayementKindId int, -- rodzaj odpłatności: 100%, 30%, ryczałt
    constraint PKPatientMedicineId primary key (Id)
    );

-- tabela z wystawionymi receptami
-- leki zamawia pacjent. Tymczasowo są zapisywane w tabeli PatientPrescription jako recepta
-- a zawartość recepty zapisywana jest w tabeli 
create table zozb.dbo.Prescription(
    Id int not null identity(1, 1),
    PrescriptionNumber int default null, -- faktyczny numer recepty
    PatientId int,
    PrescriptionDate Date default null,
    Issued int default null, -- oznacza wystawioną receptę
    constraint PKPatientPrescriptionId primary key (Id)
    );


-- tabela z lekami zamówionymi i wystawionymi pacjentowi
create table zozb.dbo.MedicinePrescription(
    Id int not null identity(1, 1),
    MedicineId int,
    MedicineCount int,
    PrescriptionId int,
    PaymentKindId int, -- rodzaj refundacji 100%, 30%, ryczałt
    constraint PKPrescriptionId primary key (Id)
    );

  
  
-- tabela zawierająca godziny wizyt pacjentów
create table zozb.dbo.PatientCalendar(
    Id int not null identity(1, 1),
    HealthCenterId int not null,
    PatientId int not null,
    PhisicanId int not null,
    StartDateTime smalldatetime not null,
    FinishDateTime smalldatetime not null,
    VisitDid int default 0, -- określa czy wizyta odbyta (1) czy nie (0)
    constraint PKPatientCalendarId primary key (Id)
    );

create table zozb.dbo.PhisicanCalendarTemplate(
    Id int not null identity(1, 1),
    PhisicanId int not null,
    HealthCenterId int not null,
    DayId int, -- null oznacza, że w danym dniu lekarz nie przyjmuje
    StartWorkTime time(0) not null,
    FinishWorkTime time(0) not null,
    constraint PKPhisicanTimeTableTemplateId primary key (Id)
    );


-- tworzenie kluczy opcych
-- tworzymy po utworzeniu tabel, ponieważ wtedy nie ma znaczenia kolejność ich tworzenia

alter table zozb.dbo.Patient
    add constraint FKPNFZRegionCodeId foreign key (NFZCodeId) references zozb.dbo.NFZRegionCode (Id);
alter table zozb.dbo.Medicine
    add constraint FKMCategoryId foreign key (CategoryId) references zozb.dbo.MedicineCategory (Id);
alter table zozb.dbo.PatientMedicine
	add constraint FKPMPatientId foreign key (PatientId) references zozb.dbo.Patient (Id);
alter table zozb.dbo.PatientMedicine
	add constraint FKPMMedicineId foreign key (MedicineId) references zozb.dbo.Medicine (Id);
alter table zozb.dbo.Prescription
	add constraint FKPPaitentId foreign key (PatientId) references zozb.dbo.Prescription (Id);
alter table zozb.dbo.MedicinePrescription
	add constraint FKMPMedicineId foreign key (MedicineId) references zozb.dbo.Medicine (Id);
alter table zozb.dbo.MedicinePrescription
	add constraint FKMPPrescriptionId foreign key (PrescriptionId) references zozb.dbo.Prescription (Id);

alter table zozb.dbo.OpeningHour
    add constraint FKOHHealthcenterId foreign key (HealthCenterId) references zozb.dbo.HealthCenter (Id);
alter table zozb.dbo.HealthCenterMail
	add constraint FKHCMHealthCenterId foreign key (HealthCenterId) references zozb.dbo.HealthCenter (Id);
alter table zozb.dbo.HealthCenterPhone
	add constraint FKHCPHealthCenterId foreign key (HealthCenterId) references zozb.dbo.HealthCenter (Id);
alter table zozb.dbo.HealthCenterClinic
	add constraint FKHCCHealthCenterId foreign key (HealthCenterId) references zozb.dbo.HealthCenter (Id);
alter table zozb.dbo.HealthCenterClinic
	add constraint FKHCCClinicId foreign key (ClinicId) references zozb.dbo.Clinic (Id);

alter table zozb.dbo.PhisicanSpecialization
	add constraint FKPSPhisicanId foreign key (PhisicanId) references zozb.dbo.Phisican (Id);
alter table zozb.dbo.PhisicanSpecialization
	add constraint FKPSSpecializationNameId foreign key (SpecializationNameId) references zozb.dbo.PhisicanSpecializationName (Id);
alter table zozb.dbo.PhisicanOpinion
	add constraint FKPOPhisicanId foreign key (PhisicanId) references zozb.dbo.Phisican (Id);
alter table zozb.dbo.PhisicanHealthCenter
	add constraint FKPHCPhisicanId foreign key (PhisicanId) references zozb.dbo.Phisican (Id);
alter table zozb.dbo.PhisicanHealthCenter
	add constraint FKPHChealthCenterId foreign key (HealthCenterId) references zozb.dbo.HealthCenter (Id);

alter table zozb.dbo.PhisicanCalendarTemplate
	add constraint FKPPhisicanId foreign key (PhisicanId) references zozb.dbo.Phisican (Id);
alter table zozb.dbo.PatientCalendar
    add constraint FKPVPatientId foreign key (PatientId) references zozb.dbo.Patient (Id);
alter table zozb.dbo.PatientCalendar
	add constraint FKPVPhisicanId foreign key (PhisicanId) references zozb.dbo.Phisican (Id);
