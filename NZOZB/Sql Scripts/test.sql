﻿delete from zozb.dbo.PhisicanOpinion

go

create procedure dbo.ZozbTestDataCreatePhisicanOpinion
    @PhisicanId int = 0
as
begin
	declare @Opinion nvarchar(max)
	declare @Rate int 
  set @Rate = abs(checksum(newid()) % 11)
	declare @FromDate datetime2 = '2017-06-01'
	declare @DateOffset int
	set @DateOffset = Rand(checksum(newid())) * -2678
	declare @OpinionDate datetime2 = dateadd(day, @Dateoffset, @FromDate)
	set @Opinion = 'Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.'
	insert into zozb.dbo.PhisicanOpinion(PhisicanId, Opinion, Rate, OpinionDate)
		values(@PhisicanId, @Opinion, @Rate, @OpinionDate)
end

go
select * from zozb.dbo.PhisicanOpinion