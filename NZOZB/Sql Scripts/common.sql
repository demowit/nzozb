CREATE PROCEDURE dbo.DeleteForeignKey
    @DatabaseName nvarchar(50),
    @KeyName nvarchar(50),
	@TableName nvarchar(50)
AS
    if exists (select 1 from sys.objects where 
		object_id = object_id(@KeyName) and parent_object_id = object_id(@TableName))
		alter table (@TableName) drop constraint (@KeyName)
RETURN 0 