﻿alter table [zozb].[dbo].[PatientMedicine]
	add constraint [FKPMPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Patient] (Id);
alter table [zozb].[dbo].[PatientMedicine]
	add constraint [FKPMMedicineId] foreign key ([MedicineId]) references [zozb].[dbo].[Medicine] (Id);
alter table [zozb].[dbo].[Prescription]
	add constraint [FKPPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Prescription] (Id);
alter table [zozb].[dbo].[MedicinePrescription]
	add constraint [FKMPMedicineId] foreign key ([MedicineId]) references [zozb].[dbo].[Medicine] (Id);
alter table [zozb].[dbo].[MedicinePrescription]
	add constraint [FKMPPrescriptionId] foreign key ([PrescriptionId]) references [zozb].[dbo].[Prescription] (Id);

alter table [zozb].[dbo].[OpeningHour]
    add constraint [FKOHHealthcenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterMail]
	add constraint [FKHCMHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterPhone]
	add constraint [FKHCPHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterClinic]
	add constraint [FKHCCHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterClinic]
	add constraint [FKHCCClinicId] foreign key ([ClinicId]) references [zozb].[dbo].[Clinic] (Id);

alter table [zozb].[dbo].[PhisicanSpecialization]
	add constraint [FKPSPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanSpecialization]
	add constraint [FKPSSpecializationNameId] foreign key ([SpecializationNameId]) references [zozb].[dbo].[PhisicanSpecializationName] (Id);
alter table [zozb].[dbo].[PhisicanOpinion]
	add constraint [FKPOPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanHealthCenter]
	add constraint [FKPHCPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanHealthCenter]
	add constraint [FKPHChealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);

alter table [zozb].[dbo].[PhisicanCalendarTemplate]
	add constraint [FKPPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PatientCalendar]
    add constraint [FKPVPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Patient] (Id);
alter table [zozb].[dbo].[PatientCalendar]
	add constraint [FKPVPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);

go

use zozb
declare @TableName nvarchar(128)
declare @FKName nvarchar(128)
declare fkCursor cursor for select a.name FKName, b.name TableName from sys.foreign_keys a join sys.objects b on a.parent_object_id = b.object_id order by TableName
open fkCursor
fetch next from fkCursor into  @FKName, @TableName
while @@FETCH_STATUS = 0
begin
	exec ('alter table ' + @TableName + ' drop constraint ' + @FKName)		
fetch next from fkCursor into @FKName, @TableName
end
close fkCursor
deallocate fkCursor



use zozb
declare @TableName nvarchar(128)
declare @FKName nvarchar(128)
declare fkCursor cursor for select a.name FKName, b.name TableName from sys.foreign_keys a join sys.objects b on a.parent_object_id = b.object_id order by TableName
open fkCursor
fetch next from fkCursor into  @FKName, @TableName
while @@FETCH_STATUS = 0
begin
	exec ('alter table ' + @TableName + ' drop constraint ' + @FKName)		
fetch next from fkCursor into @FKName, @TableName
end
close fkCursor
deallocate fkCursor
go

declare @SchemaName nvarchar(128) = 'dbo'
declare @ProcedureName nvarchar(128)
declare procCursor cursor for select a.name ProcedureName from sys.procedures a
open procCursor
fetch next from procCursor into @ProcedureName
while @@FETCH_STATUS = 0
begin
	exec ('drop procedure ' + @SchemaName + '.' + @ProcedureName)		
fetch next from procCursor into @ProcedureName
end
close procCursor
deallocate procCursor
go

declare @SchemaName nvarchar(128) = 'dbo'
declare @ProcedureName nvarchar(128)
declare procCursor cursor for select a.name ProcedureName from sys.procedures a
open procCursor
fetch next from procCursor into @ProcedureName
while @@FETCH_STATUS = 0
begin
	exec ('drop procedure ' + @SchemaName + '.' + @ProcedureName)		
fetch next from procCursor into @ProcedureName
end
close procCursor
deallocate procCursor
go

declare @SchemaName nvarchar(128) = 'dbo'
declare @TableName nvarchar(128)
declare tableCursor cursor for select a.name TableName from sys.tables a where type ='U'
open tableCursor
fetch next from tableCursor into @TableName
while @@FETCH_STATUS = 0
begin
	exec ('drop table ' + @SchemaName + '.' + @TableName)		
fetch next from tableCursor into @TableName
end
close tableCursor
deallocate tableCursor
go

declare @SchemaName nvarchar(128) = 'dbo'
declare @TableName nvarchar(128)
declare tableCursor cursor for select a.name TableName from sys.tables a where type ='U'
open tableCursor
fetch next from tableCursor into @TableName
while @@FETCH_STATUS = 0
begin
	exec ('drop table ' + @SchemaName + '.' + @TableName)		
fetch next from tableCursor into @TableName
end
close tableCursor
deallocate tableCursor
go
