﻿-- procedury testowe, tworzące dane testowe.
-- procedura tworzy tabelę z szablonem kalendarza wizyt dla lekarzy
create procedure dbo.ZozbTestDataCreatePhisicanCalendarTemplate
as
begin
	declare @WeekDay table (DayId int)
	declare @PhisicanId int
	declare @HealthCenterId int
	declare @DayNo int
	declare @StartTime time(0)
	declare @FinishTime time(0)
	declare @CurrentLoop int
	declare @OldCenterId int
	set @OldCenterId = 0
	set @CurrentLoop = 0
	declare @CenterNo int
	set @CenterNo = 0
	declare @PhisicanId1 int
	set @PhisicanId = 0
	insert @WeekDay (DayId) values (1), (2), (3), (4), (5), (6);  
	declare dbCursorCenterNo cursor for select PhisicanId, count(HealthCenterId) CenterNo 
		from zozb.dbo.PhisicanHealthCenter
		group by PhisicanId
	open dbCursorCenterNo
	fetch next from dbCursorCenterNo into @PhisicanId1, @CenterNo
	while @@FETCH_STATUS = 0
	begin
		declare dbCursor cursor for select result.PhisicanId, result.HealthCenterId,  f.DayId DayNo 
			from @WeekDay f, (
			select a.PhisicanId, a.HealthCenterId from zozb.dbo.PhisicanHealthCenter a
			join zozb.dbo.Phisican b on a.PhisicanId = b.Id 
			join zozb.dbo.HealthCenter c on a.HealthCenterId = c.Id 
			) result where result.PhisicanId = @PhisicanId1 order by result.HealthCenterId, DayNo
		open dbCursor
		fetch next from dbCursor into @PhisicanId, @HealthCenterId, @DayNo
		set @CurrentLoop = 1
		while @@FETCH_STATUS = 0
		begin
			if @HealthCenterId != @OldCenterId
			begin
				set @OldCenterId = @HealthCenterId
				if @CurrentLoop = 1
				begin
					set @StartTime = '08:00:00'
					set @FinishTime = '12:00:00'
				end
				if @CurrentLoop = 2
				begin
					set @StartTime = '12:00:00'
					set @FinishTime = '16:00:00'
				end
				if @CurrentLoop = 3
				begin
					set @StartTime = '16:00:00'
					set @FinishTime = '18:00:00'
				end
				set @CurrentLoop = @CurrentLoop + 1
			end
			insert into zozb.dbo.PhisicanCalendarTemplate(DayId, PhisicanId, HealthCenterId, StartWorkTime, FinishWorkTime) values
				(@DayNo, @PhisicanId, @HealthCenterId, @StartTime, @FinishTime)
			fetch next from dbCursor into @PhisicanId, @HealthCenterId, @DayNo
		end			
		close dbCursor
		deallocate dbCursor
		fetch next from dbCursorCenterNo into @PhisicanId1, @CenterNo
	end
	close dbCursorCenterNo
	deallocate dbCursorCenterNo
end

go

-- procedura wypełnia danymi testowymi tabelę PhisicanHealthCenter
create procedure dbo.ZozbTestDataCreatePhisicanHealthCenter
as
begin
	declare @MaxPhisicanNo int
	declare @MaxHealthCenterNo int
	select @MaxPhisicanNo = count(Id) from zozb.dbo.Phisican
	select @MaxHealthCenterNo = count(Id) from zozb.dbo.HealthCenter

	declare @CurrentPhisicanNo int = 1
	while @CurrentPhisicanNo <= @MaxPhisicanNo
	begin
		declare @CurrentMaxHealthCenterNo int = abs(checksum(newid()) % @MaxHealthCenterNo) + 1
		insert into zozb.dbo.PhisicanHealthCenter(PhisicanId, HealthCenterId)
			select top(@CurrentMaxHealthCenterNo) (@CurrentPhisicanNo), Id from zozb.dbo.HealthCenter order by newid()
		set @CurrentPhisicanNo = @CurrentPhisicanNo + 1
	end
end

go

create procedure dbo.ZozbTestDataCreatePhisicanOpinion
    @PhisicanId int = 0
as
begin
	declare @Opinion nvarchar(max)
	declare @Rate int 
	set @Rate = abs(checksum(newid()) % 11)
	declare @FromDate datetime2 = '2017-06-01'
	declare @DateOffset int
	set @DateOffset = Rand(checksum(newid())) * -2678
	declare @OpinionDate datetime2 = dateadd(day, @Dateoffset, @FromDate)
	set @Opinion = 'Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.'
	insert into zozb.dbo.PhisicanOpinion(PhisicanId, Opinion, Rate, OpinionDate)
		values(@PhisicanId, @Opinion, @Rate, @OpinionDate)
end

go

-- procedura odczytuje lekarzy i pryporządkowuje im specjalizację. Minimum 1, maksumum 3
create procedure dbo.ZozbTestDataCreatePhisicanSpecialization
as
begin
	declare @MaxPhisicanNo int = 0
	declare @MaxSpecializationNo int = 0
	select @MaxPhisicanNo = count(Id) from zozb.dbo.Phisican
	select @MaxSpecializationNo = count(Id) from zozb.dbo.PhisicanSpecializationName
	declare @CurrentSpecializationNo int = 1
	declare @CurrentPhisicanNo int = 1
	declare @TempSpecializationTable table (SpecializationNo int, NotFinished int)
	while @CurrentPhisicanNo <= @MaxPhisicanNo
	begin
    delete from @TempSpecializationTable
    set @CurrentSpecializationNo = 1
    declare @NotFinished int 
    while @CurrentSpecializationNo <= @MaxSpecializationNo
    begin      
      if @CurrentSpecializationNo > 1
      begin
        set @NotFinished = abs(checksum(newid()) % 2) 
      end
      else
      begin
        set @NotFinished = 0
      end
      insert into @TempSpecializationTable values(@CurrentSpecializationNo, @NotFinished)
      set @CurrentSpecializationNo = @CurrentSpecializationNo + 1
    end    
    declare @MaxSpecializationNoLoop int
    set @MaxSpecializationNoLoop = abs(checksum(newid()) % 3) + 1
    insert into zozb.dbo.PhisicanSpecialization (PhisicanId, SpecializationNameId, NotFinished) 
      select top (@MaxSpecializationNoLoop) @CurrentPhisicanNo, SpecializationNo,  NotFinished from @TempSpecializationTable order by newid()
		set @CurrentPhisicanNo = @CurrentPhisicanNo + 1
	end
end

go

create procedure dbo.RemoveVisit @VisitId int = null as 
begin
    if @VisitId != null 
    begin
        delete from zozb.dbo.PatientVisit where Id = @VisitId
    end
end

go

create procedure dbo.CommitVisit @VisitId int = null as
begin
    if @VisitId != null
    begin
        update zozb.dbo.PatientVisit set VisitDid = 1 where Id = @VisitId
    end
end

go

create procedure dbo.NewVisit 
	@HealthCenterId int,
	@PhisicanId int,
	@PatientId int,
	@StartDateTime datetime2,
	@FinishDateTime datetime2
as
begin
	insert into zozb.dbo.PatientVisit(HealthCenterId, PhisicanId, PatientId, StartDateTime, FinishDateTime)
		values(@HealthCenterId, @PhisicanId, @PatientId, @StartDateTime, @FinishDateTime)
end

