﻿--Terminologia:
--  Przychodnia HealthCenter
--  Poradnia Clinic
--  Lekarz Phisican
--  Pacjent Patient
--  Kalendarz Calendar
--  Otwarcie Opening
--  Rejestracja Registering
--  Specializacja Specialization
--  Specializacja kliniki ClinicSpecialization
--  Specialiacja lekarza PhisicanSpecialization
--  Lekarstwa Medicine
--  Recepty Prescription
--
-- Struktura Firmy:
--  
--  Przychodnia 1
--    Poradnia 11
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 12
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 1N
--      Lekarz 
--      Lekarz 
--      Lekarz 
--  
--  Przychodnia 2
--    Poradnia 21
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 22
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia 2N
--      Lekarz 
--      Lekarz 
--      Lekarz 
--  
--  Przychodnia N
--    Poradnia N1
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia N2
--      Lekarz 
--      Lekarz 
--      Lekarz 
--    Poradnia NN
--      Lekarz 
--      Lekarz 
--      Lekarz 
--
-- Tabele podstawowe:

-- tabela zawiera nazwy i adresy przychodni

use zozb

go



declare @TableName nvarchar(128)
declare @FKName nvarchar(128)
declare fkCursor cursor for select a.name FKName, b.name TableName from sys.foreign_keys a join sys.objects b on a.parent_object_id = b.object_id order by TableName
open fkCursor
fetch next from fkCursor into  @FKName, @TableName
while @@FETCH_STATUS = 0
begin
	exec ('alter table ' + @TableName + ' drop constraint ' + @FKName)		
fetch next from fkCursor into @FKName, @TableName
end
close fkCursor
deallocate fkCursor
go


declare @SchemaName nvarchar(128) = 'dbo'
declare @ProcedureName nvarchar(128)
declare procCursor cursor for select a.name ProcedureName from sys.procedures a
open procCursor
fetch next from procCursor into @ProcedureName
while @@FETCH_STATUS = 0
begin
	exec ('drop procedure ' + @SchemaName + '.' + @ProcedureName)		
fetch next from procCursor into @ProcedureName
end
close procCursor
deallocate procCursor
go

declare @SchemaName nvarchar(128) = 'dbo'
declare @TableName nvarchar(128)
declare tableCursor cursor for select a.name TableName from sys.tables a where type ='U'
open tableCursor
fetch next from tableCursor into @TableName
while @@FETCH_STATUS = 0
begin
	exec ('drop table ' + @SchemaName + '.' + @TableName)		
fetch next from tableCursor into @TableName
end
close tableCursor
deallocate tableCursor
go

create table [zozb].[dbo].[HealthCenter](
	[Id] int not null identity(1, 1),
	[CenterName] nvarchar(100),
	[CenterDescription] nvarchar(max),
	Street nvarchar(50),
	[PostCode] nvarchar(6),
	[City] nvarchar(50),
	constraint [PKHealthCenterId] primary key (Id)
	);

  
-- tabela zawiera nazwy poradni ich lokalizację w przychodni
create table [zozb].[dbo].[Clinic](
    [Id] int not null identity(1, 1),
    [ClinicName] nvarchar(50),
    [ClinicDescription] nvarchar(max),
    constraint [PKClinicId] primary key (Id)
    );


  
-- tabele asocjacyjne dla Przychodni i Poradni
-- Przychodnia - Poradnia Jedna przychodnia - wiele poradni. Jedna poradnia - jedna przychodnia

create table [zozb].[dbo].[HealthCenterClinic](
    [Id] int not null identity(1, 1),
    [HealthCenterId] int,
    [ClinicId] int,
    constraint [PKHealthCenterClinicId] primary key (Id)
    );

  
-- tabela z numerami telefonów
create table [zozb].[dbo].[HealthCenterPhone](
    [Id] int not null identity(1, 1),
    [HealthCenterId] int, -- id przychodni lub poradni
    [Number] nvarchar(12),
    [NumberDescription] nvarchar(200)
    constraint [PKHealthCenterPhoneId] primary key (Id)
    );

-- tabela z numerami maili przychodni
create table [zozb].[dbo].[HealthCenterMail](
    [Id] int not null identity(1, 1),
    [HealthCenterId] int, -- id przychodni lub poradni
    [Address] nvarchar(320),
    [AddressDescription] nvarchar(200),
    constraint [PKHealthCenterMailId] primary key (Id)
    );

-- tabela zawierająca godziny otwarcia przychodni
create table [zozb].[dbo].[OpeningHour](
    [Id] int not null identity(1, 1),
    [HealthCenterId] int, -- id przychodni lub poradni
    [StartTime] time,
    [FinishTime] time,
    [DayNumber] int, -- numer dnia (pn, wt, itp) 
    constraint [PKOpeningHourId] primary key (Id)
    );

-- nazwiska lekarzy
create table [zozb].[dbo].[Phisican](
	[Id] int not null identity(1,1),
	[FirstName] nvarchar(50),
	[LastName] nvarchar(100),
	[PhotoLink] nvarchar(2000) default null,
	[VisitTimeLength] numeric(5, 0),
	constraint [PKPhisicanId] primary key (Id)
	);

-- nazwy specjalizacji lekarskich
create table [zozb].[dbo].[PhisicanSpecializationName](
	[Id] int not null identity(1,1),
	[SpecializationName] nvarchar(50),
	[Description] nvarchar(max)
	constraint [PKPhisicanSpecializationName] primary key (Id)
	);

-- specjalizacja lekarza. Jeden lekarz - wiele specjalizacji
create table [zozb].[dbo].[PhisicanSpecialization](
	[Id] int not null identity (1,1),
	[PhisicanId] int,
	[SpecializationNameId] int,
	[NotFinished] tinyint default 0, 
	constraint [PKPhisicanSpecialization] primary key (Id),
	);


-- opinie o lekarzach
create table [zozb].[dbo].[PhisicanOpinion](
	[Id] int not null identity(1,1),
	[PhisicanId] int,
	[Opinion] nvarchar(max),
	[Rate] int,
	[OpinionDate] date
	constraint [PKPhisicanOpinionId] primary key (Id),
	);

create table [zozb].[dbo].[PhisicanHealthCenter](
	[Id] int not null identity(1, 1),
	[PhisicanId] int,
	[HealthCenterId] int,
	constraint [PKPhisicanHealthCenterId] primary key clustered (Id)
	);


-- Tabela z pacjentami.
-- Na podstawie danych z tabeli generowany jest dokument przystąpinia do przychodni
-- może zostać on wydrukowany lub zapisany jako pdf na dysku

create table [zozb].[dbo].[Patient](
	[Id] int not null identity(1, 1),
	[FirstName] nvarchar(50) not null,
	[LastName] nvarchar(100) not null,
	[MotherFamilyName] nvarchar(50) not null,
	[BirthDate] Date not null,
	[Sex] int not null, -- 0 male 1 female
	[Pesel] nvarchar(11),
	[Street] nvarchar(50),
	[City] nvarchar(50),
	[PostCode] nvarchar(6),
	[PhoneNumber] nvarchar(12),
	[MailAddress] nvarchar(320),
	[InsuranceCardNumber] nvarchar(50), -- może być pusta null
	[NFZRegionCodeId] int not null,
	[RegisterDate] Date not null,
	[PhisicanId] int, -- kod lekarza podstawowej opieki lekarskiej. może być pusty (null)
	[HealthCenterId] int, -- kod przychodni do której zapisany jest pacjent
	constraint [PKPatientId] primary key (Id)
	);

create table [zozb].[dbo].[NFZRegionCode](
    [Id] int not null identity(1, 1),
    [NFZRegionCode] nvarchar(2) not null,
    [Region] nvarchar(200),
    constraint [PKNFZRegionCodeId] primary key (Id)
    );


-- tabele dotyczące lekarstw
-- rodzaje odpłatności za leki
create table [zozb].[dbo].[RefundKind](
    [Id] int not null identity(1, 1),
    [RefundName] nvarchar(50),
    constraint [PKRefundKindId] primary key (Id)
    );

-- tabela z kategoriami lekarstw
create table [zozb].[dbo].[MedicineCategory](
    [Id] int not null identity(1, 1),
    [CategoryName] nvarchar(100),
    constraint [PKMedicineCategoryId] primary key (Id)
    );

-- tabela z lekarstwami. spełnia tę samą rolę co tabela z produktami w sklepie.
create table [zozb].[dbo].[Medicine](
    [Id] int not null identity(1, 1),
    [CategoryId] int,
    [MedicineName] nvarchar(100),
    [MedicineDescription] nvarchar(max),
    constraint [PKMedicineId] primary key (Id)
    );

-- tabele asocjacyjne.
-- tabela leków przepisanych pacjentowi przez lekarza. Na jej podstawie pacjent generuje zamówienie na receptę.
create table [zozb].[dbo].[PatientMedicine](
    [Id] int not null identity(1, 1),
    [PatientId] int,
    [MedicineId] int,
    [Dosage] nvarchar(200),
    [RefundKindId] int, -- rodzaj odpłatności: 100%, 30%, ryczałt
    constraint [PKPatientMedicineId] primary key (Id)
    );

-- tabela z wystawionymi receptami
-- leki zamawia pacjent. Tymczasowo są zapisywane w tabeli PatientPrescription jako recepta
-- a zawartość recepty zapisywana jest w tabeli 
create table [zozb].[dbo].[Prescription](
    [Id] int not null identity(1, 1),
    [PrescriptionNumber] int default null, -- faktyczny numer recepty
    [PatientId] int,
    [PrescriptionDate] Date default null,
    [Issued] int default null, -- oznacza wystawioną receptę
    constraint [PKPatientPrescriptionId] primary key (Id)
    );


-- tabela z lekami zamówionymi i wystawionymi pacjentowi
create table [zozb].[dbo].[MedicinePrescription](
    [Id] int not null identity(1, 1),
    [MedicineId] int,
    [MedicineCount] int,
    [PrescriptionId] int,
    [PaymentKindId] int, -- rodzaj refundacji 100%, 30%, ryczałt
    constraint [PKPrescriptionId] primary key (Id)
    );

  
  
-- tabela zawierająca godziny wizyt pacjentów
create table [zozb].[dbo].[PatientCalendar](
    [Id] int not null identity(1, 1),
    [HealthCenterId] int not null,
    [PatientId] int not null,
    [PhisicanId] int not null,
    [StartDateTime] smalldatetime not null,
    [FinishDateTime] smalldatetime not null,
    [VisitCompleted] int default 0, -- określa czy wizyta odbyta (1) czy nie (0)
    constraint [PKPatientCalendarId] primary key (Id)
    );

create table [zozb].[dbo].[PhisicanCalendarTemplate](
    [Id] int not null identity(1, 1),
    [PhisicanId] int not null,
    [HealthCenterId] int not null,
    [DayId] int, -- null oznacza, że w danym dniu lekarz nie przyjmuje
    [StartWorkTime] time(0) not null,
    [FinishWorkTime] time(0) not null,
    constraint [PKPhisicanTimeTableTemplateId] primary key (Id)
    );


-- tworzenie kluczy opcych
-- tworzymy po utworzeniu tabel, ponieważ wtedy nie ma znaczenia kolejność ich tworzenia

if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKPNFZRegionCodeId]') and parent_object_id = object_id('[Patient]')) 
  alter table [Patient] drop constraint [FKPNFZRegionCodeId]
if exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[FKMCategoryId]') and parent_object_id = object_id('[Medicine]')) 
  alter table [Medicine] drop constraint [FKMCategoryId]


alter table [zozb].[dbo].[Patient]
    add constraint [FKPNFZRegionCodeId] foreign key ([NFZRegionCodeId]) references [zozb].[dbo].[NFZRegionCode] (Id);
alter table [zozb].[dbo].[Medicine]
    add constraint [FKMCategoryId] foreign key ([CategoryId]) references [zozb].[dbo].[MedicineCategory] (Id);
alter table [zozb].[dbo].[PatientMedicine]
	add constraint [FKPMPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Patient] (Id);
alter table [zozb].[dbo].[PatientMedicine]
	add constraint [FKPMMedicineId] foreign key ([MedicineId]) references [zozb].[dbo].[Medicine] (Id);
alter table [zozb].[dbo].[Prescription]
	add constraint [FKPPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Prescription] (Id);
alter table [zozb].[dbo].[MedicinePrescription]
	add constraint [FKMPMedicineId] foreign key ([MedicineId]) references [zozb].[dbo].[Medicine] (Id);
alter table [zozb].[dbo].[MedicinePrescription]
	add constraint [FKMPPrescriptionId] foreign key ([PrescriptionId]) references [zozb].[dbo].[Prescription] (Id);

alter table [zozb].[dbo].[OpeningHour]
    add constraint [FKOHHealthcenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterMail]
	add constraint [FKHCMHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterPhone]
	add constraint [FKHCPHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterClinic]
	add constraint [FKHCCHealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);
alter table [zozb].[dbo].[HealthCenterClinic]
	add constraint [FKHCCClinicId] foreign key ([ClinicId]) references [zozb].[dbo].[Clinic] (Id);

alter table [zozb].[dbo].[PhisicanSpecialization]
	add constraint [FKPSPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanSpecialization]
	add constraint [FKPSSpecializationNameId] foreign key ([SpecializationNameId]) references [zozb].[dbo].[PhisicanSpecializationName] (Id);
alter table [zozb].[dbo].[PhisicanOpinion]
	add constraint [FKPOPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanHealthCenter]
	add constraint [FKPHCPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PhisicanHealthCenter]
	add constraint [FKPHChealthCenterId] foreign key ([HealthCenterId]) references [zozb].[dbo].[HealthCenter] (Id);

alter table [zozb].[dbo].[PhisicanCalendarTemplate]
	add constraint [FKPPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);
alter table [zozb].[dbo].[PatientCalendar]
    add constraint [FKPVPatientId] foreign key ([PatientId]) references [zozb].[dbo].[Patient] (Id);
alter table [zozb].[dbo].[PatientCalendar]
	add constraint [FKPVPhisicanId] foreign key ([PhisicanId]) references [zozb].[dbo].[Phisican] (Id);


go

-- procedury testowe, tworzące dane testowe.
-- procedura tworzy tabelę z szablonem kalendarza wizyt dla lekarzy
create procedure dbo.ZozbTestDataCreatePhisicanCalendarTemplate
as
begin
	declare @WeekDay table (DayId int)
	declare @PhisicanId int
	declare @HealthCenterId int
	declare @DayNo int
	declare @StartTime time(0)
	declare @FinishTime time(0)
	declare @CurrentLoop int
	declare @OldCenterId int
	set @OldCenterId = 0
	set @CurrentLoop = 0
	declare @CenterNo int
	set @CenterNo = 0
	declare @PhisicanId1 int
	set @PhisicanId = 0
	insert @WeekDay (DayId) values (1), (2), (3), (4), (5), (6);  
	declare dbCursorCenterNo cursor for select PhisicanId, count(HealthCenterId) CenterNo 
		from zozb.dbo.PhisicanHealthCenter
		group by PhisicanId
	open dbCursorCenterNo
	fetch next from dbCursorCenterNo into @PhisicanId1, @CenterNo
	while @@FETCH_STATUS = 0
	begin
		declare dbCursor cursor for select result.PhisicanId, result.HealthCenterId,  f.DayId DayNo 
			from @WeekDay f, (
			select a.PhisicanId, a.HealthCenterId from zozb.dbo.PhisicanHealthCenter a
			join zozb.dbo.Phisican b on a.PhisicanId = b.Id 
			join zozb.dbo.HealthCenter c on a.HealthCenterId = c.Id 
			) result where result.PhisicanId = @PhisicanId1 order by result.HealthCenterId, DayNo
		open dbCursor
		fetch next from dbCursor into @PhisicanId, @HealthCenterId, @DayNo
		set @CurrentLoop = 1
		while @@FETCH_STATUS = 0
		begin
			if @HealthCenterId != @OldCenterId
			begin
				set @OldCenterId = @HealthCenterId
				if @CurrentLoop = 1
				begin
					set @StartTime = '08:00:00'
					set @FinishTime = '12:00:00'
				end
				if @CurrentLoop = 2
				begin
					set @StartTime = '12:00:00'
					set @FinishTime = '16:00:00'
				end
				if @CurrentLoop = 3
				begin
					set @StartTime = '16:00:00'
					set @FinishTime = '18:00:00'
				end
				set @CurrentLoop = @CurrentLoop + 1
			end
			insert into zozb.dbo.PhisicanCalendarTemplate(DayId, PhisicanId, HealthCenterId, StartWorkTime, FinishWorkTime) values
				(@DayNo, @PhisicanId, @HealthCenterId, @StartTime, @FinishTime)
			fetch next from dbCursor into @PhisicanId, @HealthCenterId, @DayNo
		end			
		close dbCursor
		deallocate dbCursor
		fetch next from dbCursorCenterNo into @PhisicanId1, @CenterNo
	end
	close dbCursorCenterNo
	deallocate dbCursorCenterNo
end

go

-- procedura wypełnia danymi testowymi tabelę PhisicanHealthCenter
create procedure dbo.ZozbTestDataCreatePhisicanHealthCenter
as
begin
	declare @MaxPhisicanNo int
	declare @MaxHealthCenterNo int
	select @MaxPhisicanNo = count(Id) from zozb.dbo.Phisican
	select @MaxHealthCenterNo = count(Id) from zozb.dbo.HealthCenter

	declare @CurrentPhisicanNo int = 1
	while @CurrentPhisicanNo <= @MaxPhisicanNo
	begin
		declare @CurrentMaxHealthCenterNo int = abs(checksum(newid()) % @MaxHealthCenterNo) + 1
		insert into zozb.dbo.PhisicanHealthCenter(PhisicanId, HealthCenterId)
			select top(@CurrentMaxHealthCenterNo) (@CurrentPhisicanNo), Id from zozb.dbo.HealthCenter order by newid()
		set @CurrentPhisicanNo = @CurrentPhisicanNo + 1
	end
end

go

create procedure dbo.ZozbTestDataCreatePhisicanOpinion
    @PhisicanId int = 0
as
begin
	declare @Opinion nvarchar(max)
	declare @Rate int 
	set @Rate = abs(checksum(newid()) % 11)
	declare @FromDate datetime2 = '2017-06-01'
	declare @DateOffset int
	set @DateOffset = Rand(checksum(newid())) * -2678
	declare @OpinionDate datetime2 = dateadd(day, @Dateoffset, @FromDate)
	set @Opinion = 'Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.
		Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum. Lorem ipsum.'
	insert into zozb.dbo.PhisicanOpinion(PhisicanId, Opinion, Rate, OpinionDate)
		values(@PhisicanId, @Opinion, @Rate, @OpinionDate)
end

go

-- procedura odczytuje lekarzy i pryporządkowuje im specjalizację. Minimum 1, maksumum 3
create procedure dbo.ZozbTestDataCreatePhisicanSpecialization
as
begin
	declare @MaxPhisicanNo int = 0
	declare @MaxSpecializationNo int = 0
	select @MaxPhisicanNo = count(Id) from zozb.dbo.Phisican
	select @MaxSpecializationNo = count(Id) from zozb.dbo.PhisicanSpecializationName
	declare @CurrentSpecializationNo int = 1
	declare @CurrentPhisicanNo int = 1
	declare @TempSpecializationTable table (SpecializationNo int, NotFinished int)
	while @CurrentPhisicanNo <= @MaxPhisicanNo
	begin
    delete from @TempSpecializationTable
    set @CurrentSpecializationNo = 1
    declare @NotFinished int 
    while @CurrentSpecializationNo <= @MaxSpecializationNo
    begin      
      if @CurrentSpecializationNo > 1
      begin
        set @NotFinished = abs(checksum(newid()) % 2) 
      end
      else
      begin
        set @NotFinished = 0
      end
      insert into @TempSpecializationTable values(@CurrentSpecializationNo, @NotFinished)
      set @CurrentSpecializationNo = @CurrentSpecializationNo + 1
    end    
    declare @MaxSpecializationNoLoop int
    set @MaxSpecializationNoLoop = abs(checksum(newid()) % 3) + 1
    insert into zozb.dbo.PhisicanSpecialization (PhisicanId, SpecializationNameId, NotFinished) 
      select top (@MaxSpecializationNoLoop) @CurrentPhisicanNo, SpecializationNo,  NotFinished from @TempSpecializationTable order by newid()
		set @CurrentPhisicanNo = @CurrentPhisicanNo + 1
	end
end

go

create procedure dbo.RemoveVisit @VisitId int = null as 
begin
    if @VisitId != null 
    begin
        delete from zozb.dbo.PatientVisit where Id = @VisitId
    end
end

go

create procedure dbo.CommitVisit @VisitId int = null as
begin
    if @VisitId != null
    begin
        update zozb.dbo.PatientVisit set VisitDid = 1 where Id = @VisitId
    end
end

go

create procedure dbo.NewVisit 
	@HealthCenterId int,
	@PhisicanId int,
	@PatientId int,
	@StartDateTime datetime2,
	@FinishDateTime datetime2
as
begin
	insert into zozb.dbo.PatientVisit(HealthCenterId, PhisicanId, PatientId, StartDateTime, FinishDateTime)
		values(@HealthCenterId, @PhisicanId, @PatientId, @StartDateTime, @FinishDateTime)
end

