﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NZOZB.Models.Common
{
    public class Messages
    {
        public string SelectedToMailAddress { get; set; }
        public MailMessage[] Msg { get; set; }
    }
    public class MailMessage
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
    }
}