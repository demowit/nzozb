﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NZOZB.Models.Common
{
    public class ContactCards
    {
        public HealthCenterContactCard ContactCard { get; set; }
        public List<ShortCenterInformation> Centers { get; set; }
    }
}