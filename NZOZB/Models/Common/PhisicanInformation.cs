﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models.Common
{
    public class ShortPhisicanInformation
    {
        public int? PhisicanId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return LastName + " " + FirstName;
            }
        }
        public int? SpecializationNameId { get; set; } = null;
        public string SpecializationName { get; set; } = string.Empty;
        public string PhotoName { get; set; }
    }

    public class FullPhisicanInformation : ShortPhisicanInformation
    {
        public double? AverrageRate { get; set; }
        public List<PhisicanOpinion> Opinion { get; set; } = new List<PhisicanOpinion>();
    }

    public class PhisicanInformation
    {
        public List<ShortCenterInformation> Centers { get; set; } = new List<ShortCenterInformation>();
        public Dictionary<string, List<ShortPhisicanInformation>> Phisicans { get; set; } = new Dictionary<string, List<ShortPhisicanInformation>>();
        public FullPhisicanInformation FullPhisicanInformation { get; set; } = null;
    }
}