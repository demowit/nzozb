﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models.Common
{
    public class ShortClinicInformation
    {
        public int? Id { get; set; }
        public string ClinicName { get; set; }
        public string ClinicDescription { get; set; }
    }


    public class ClinicInformation
    {
        public List<ShortCenterInformation> Centers { get; set; } = new List<ShortCenterInformation>();
        public List<ShortClinicInformation> Clinics { get; set; } = new List<ShortClinicInformation>();
    }
}