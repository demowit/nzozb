﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models.Common
{
    public class Calendar
    {
        public int SpecializationId { get; set; }
        public int CenterId { get; set; }
        public int PhisicanId { get; set; }
        public List<HealthCenter> Center { get; set; }
        public List<Phisican> Phisican { get; set; }
        public List<PhisicanSpecializationName> Specialization { get; set; }
    }
}