﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models.Common
{
    public enum Sex { male = 0, female = 1}
    public class PatientData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MotherName { get; set; }
        public DateTime BirthDate { get; set; } 
        public Sex Sex { get; set; }
        public string SexName
        {
            get
            {
                switch (this.Sex)
                {
                    case Sex.male:
                        return "meżczyzna";
                    case Sex.female:
                        return "kobieta";
                }
                return "nieznana";
            }
        }
        public string Pesel { get; set; }
        public string Street { get; set; }
        public string City {  get; set; }
        public string PostCode { get; set; }
        public string PhoneNumber { get; set; }
        public string MailAddress { get; set; }
        public string InsuranceCardNumber { get; set; }
        public string PhisicanName { get; set; }
        public int? PhisicanId { get; set; }
        public int? HealthCenterId { get; set; }
        public int NFZRegionCodeId { get; set; }
        public List<ShortPhisicanInformation> Phisican { get; set; }
        public List<ShortCenterInformation> HealthCenter { get; set; }
        public List<NFZRegionCode> NFZRegionalCode { get; set; }
    }
}