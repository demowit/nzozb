﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models.Common
{
    public class HealthCenterContactCard
    {
        public int CenterId { get; set; }
        public string CenterName { get; set; }
        public string CenterDescription { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public List<HealthCenterPhone> Phone { get; set; }
        public List<HealthCenterMail> Mail { get; set; }
        public List<OpeningHour> OpeningHour { get; set; }
        public MailMessage MailMessage = new MailMessage();
        public string ReturnedMailAddress { get; set; }

    }
}