﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NZOZB.Models.Common
{
    public class ShortCenterInformation
    {
        public int CenterId { get; set; }
        public string CenterName { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
    }
}