namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NFZRegionCode")]
    public partial class NFZRegionCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NFZRegionCode()
        {
            Patient = new HashSet<Patient>();
        }

        public int Id { get; set; }

        [Column("NFZRegionCode")]
        [Required]
        [StringLength(2)]
        public string NFZRegionCode1 { get; set; }

        [StringLength(200)]
        public string Region { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Patient> Patient { get; set; }
    }
}
