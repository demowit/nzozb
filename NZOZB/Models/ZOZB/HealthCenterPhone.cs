namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HealthCenterPhone")]
    public partial class HealthCenterPhone
    {
        public int Id { get; set; }

        public int? HealthCenterId { get; set; }

        [StringLength(12)]
        public string Number { get; set; }

        [StringLength(200)]
        public string NumberDescription { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }
    }
}
