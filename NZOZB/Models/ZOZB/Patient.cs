namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Patient")]
    public partial class Patient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Patient()
        {
            PatientMedicine = new HashSet<PatientMedicine>();
            PatientCalendar = new HashSet<PatientCalendar>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string MotherFamilyName { get; set; }

        [Column(TypeName = "date")]
        public DateTime BirthDate { get; set; }

        public int Sex { get; set; }

        [StringLength(11)]
        public string Pesel { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(6)]
        public string PostCode { get; set; }

        [StringLength(12)]
        public string PhoneNumber { get; set; }

        [StringLength(320)]
        public string MailAddress { get; set; }

        [StringLength(50)]
        public string InsuranceCardNumber { get; set; }

        public int NFZRegionCodeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime RegisterDate { get; set; }

        public int? PhisicanId { get; set; }

        public int? HealthCenterId { get; set; }

        public virtual NFZRegionCode NFZRegionCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientMedicine> PatientMedicine { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientCalendar> PatientCalendar { get; set; }
    }
}
