namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OpeningHour")]
    public partial class OpeningHour
    {
        public int Id { get; set; }

        public int? HealthCenterId { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? FinishTime { get; set; }

        public int? DayNumber { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }
    }
}
