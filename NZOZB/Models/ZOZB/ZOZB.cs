namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ZOZB : DbContext
    {
        public ZOZB()
            : base("name=ZOZB")
        {
        }

        public virtual DbSet<Clinic> Clinic { get; set; }
        public virtual DbSet<HealthCenter> HealthCenter { get; set; }
        public virtual DbSet<HealthCenterClinic> HealthCenterClinic { get; set; }
        public virtual DbSet<HealthCenterMail> HealthCenterMail { get; set; }
        public virtual DbSet<HealthCenterPhone> HealthCenterPhone { get; set; }
        public virtual DbSet<Medicine> Medicine { get; set; }
        public virtual DbSet<MedicineCategory> MedicineCategory { get; set; }
        public virtual DbSet<MedicinePrescription> MedicinePrescription { get; set; }
        public virtual DbSet<NFZRegionCode> NFZRegionCode { get; set; }
        public virtual DbSet<OpeningHour> OpeningHour { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<PatientCalendar> PatientCalendar { get; set; }
        public virtual DbSet<PatientMedicine> PatientMedicine { get; set; }
        public virtual DbSet<Phisican> Phisican { get; set; }
        public virtual DbSet<PhisicanCalendarTemplate> PhisicanCalendarTemplate { get; set; }
        public virtual DbSet<PhisicanHealthCenter> PhisicanHealthCenter { get; set; }
        public virtual DbSet<PhisicanOpinion> PhisicanOpinion { get; set; }
        public virtual DbSet<PhisicanSpecialization> PhisicanSpecialization { get; set; }
        public virtual DbSet<PhisicanSpecializationName> PhisicanSpecializationName { get; set; }
        public virtual DbSet<Prescription> Prescription { get; set; }
        public virtual DbSet<RefundKind> RefundKind { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MedicineCategory>()
                .HasMany(e => e.Medicine)
                .WithOptional(e => e.MedicineCategory)
                .HasForeignKey(e => e.CategoryId);

            modelBuilder.Entity<NFZRegionCode>()
                .HasMany(e => e.Patient)
                .WithRequired(e => e.NFZRegionCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.PatientCalendar)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Phisican>()
                .Property(e => e.VisitTimeLength)
                .HasPrecision(5, 0);

            modelBuilder.Entity<Phisican>()
                .HasMany(e => e.PatientCalendar)
                .WithRequired(e => e.Phisican)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Phisican>()
                .HasMany(e => e.PhisicanCalendarTemplate)
                .WithRequired(e => e.Phisican)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PhisicanCalendarTemplate>()
                .Property(e => e.StartWorkTime)
                .HasPrecision(0);

            modelBuilder.Entity<PhisicanCalendarTemplate>()
                .Property(e => e.FinishWorkTime)
                .HasPrecision(0);

            modelBuilder.Entity<PhisicanSpecializationName>()
                .HasMany(e => e.PhisicanSpecialization)
                .WithOptional(e => e.PhisicanSpecializationName)
                .HasForeignKey(e => e.SpecializationNameId);

            modelBuilder.Entity<Prescription>()
                .HasMany(e => e.Prescription1)
                .WithOptional(e => e.Prescription2)
                .HasForeignKey(e => e.PatientId);
        }
    }
}
