namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HealthCenterMail")]
    public partial class HealthCenterMail
    {
        public int Id { get; set; }

        public int? HealthCenterId { get; set; }

        [StringLength(320)]
        public string Address { get; set; }

        [StringLength(200)]
        public string AddressDescription { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }
    }
}
