namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PatientMedicine")]
    public partial class PatientMedicine
    {
        public int Id { get; set; }

        public int? PatientId { get; set; }

        public int? MedicineId { get; set; }

        [StringLength(200)]
        public string Dosage { get; set; }

        public int? RefundKindId { get; set; }

        public virtual Medicine Medicine { get; set; }

        public virtual Patient Patient { get; set; }
    }
}
