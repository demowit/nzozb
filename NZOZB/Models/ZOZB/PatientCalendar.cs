namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PatientCalendar")]
    public partial class PatientCalendar
    {
        public int Id { get; set; }

        public int HealthCenterId { get; set; }

        public int PatientId { get; set; }

        public int PhisicanId { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime StartDateTime { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime FinishDateTime { get; set; }

        public int? VisitCompleted { get; set; }

        public virtual Patient Patient { get; set; }

        public virtual Phisican Phisican { get; set; }
    }
}
