namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MedicinePrescription")]
    public partial class MedicinePrescription
    {
        public int Id { get; set; }

        public int? MedicineId { get; set; }

        public int? MedicineCount { get; set; }

        public int? PrescriptionId { get; set; }

        public int? PaymentKindId { get; set; }

        public virtual Medicine Medicine { get; set; }

        public virtual Prescription Prescription { get; set; }
    }
}
