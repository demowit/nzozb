namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Medicine")]
    public partial class Medicine
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Medicine()
        {
            MedicinePrescription = new HashSet<MedicinePrescription>();
            PatientMedicine = new HashSet<PatientMedicine>();
        }

        public int Id { get; set; }

        public int? CategoryId { get; set; }

        [StringLength(100)]
        public string MedicineName { get; set; }

        public string MedicineDescription { get; set; }

        public virtual MedicineCategory MedicineCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicinePrescription> MedicinePrescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientMedicine> PatientMedicine { get; set; }
    }
}
