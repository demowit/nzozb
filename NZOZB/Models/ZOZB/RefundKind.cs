namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RefundKind")]
    public partial class RefundKind
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string RefundName { get; set; }
    }
}
