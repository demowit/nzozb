namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HealthCenterClinic")]
    public partial class HealthCenterClinic
    {
        public int Id { get; set; }

        public int? HealthCenterId { get; set; }

        public int? ClinicId { get; set; }

        public virtual Clinic Clinic { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }
    }
}
