namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhisicanCalendarTemplate")]
    public partial class PhisicanCalendarTemplate
    {
        public int Id { get; set; }

        public int PhisicanId { get; set; }

        public int HealthCenterId { get; set; }

        public int? DayId { get; set; }

        public TimeSpan StartWorkTime { get; set; }

        public TimeSpan FinishWorkTime { get; set; }

        public virtual Phisican Phisican { get; set; }
    }
}
