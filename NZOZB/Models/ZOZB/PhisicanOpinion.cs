namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhisicanOpinion")]
    public partial class PhisicanOpinion
    {
        public int Id { get; set; }

        public int? PhisicanId { get; set; }

        public string Opinion { get; set; }

        public int? Rate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? OpinionDate { get; set; }

        public virtual Phisican Phisican { get; set; }
    }
}
