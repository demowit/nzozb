namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HealthCenter")]
    public partial class HealthCenter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HealthCenter()
        {
            HealthCenterClinic = new HashSet<HealthCenterClinic>();
            HealthCenterMail = new HashSet<HealthCenterMail>();
            HealthCenterPhone = new HashSet<HealthCenterPhone>();
            OpeningHour = new HashSet<OpeningHour>();
            PhisicanHealthCenter = new HashSet<PhisicanHealthCenter>();
        }

        public int Id { get; set; }

        [StringLength(100)]
        public string CenterName { get; set; }

        public string CenterDescription { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [StringLength(6)]
        public string PostCode { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HealthCenterClinic> HealthCenterClinic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HealthCenterMail> HealthCenterMail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HealthCenterPhone> HealthCenterPhone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OpeningHour> OpeningHour { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanHealthCenter> PhisicanHealthCenter { get; set; }
    }
}
