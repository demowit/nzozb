namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Phisican")]
    public partial class Phisican
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Phisican()
        {
            PatientCalendar = new HashSet<PatientCalendar>();
            PhisicanHealthCenter = new HashSet<PhisicanHealthCenter>();
            PhisicanOpinion = new HashSet<PhisicanOpinion>();
            PhisicanCalendarTemplate = new HashSet<PhisicanCalendarTemplate>();
            PhisicanSpecialization = new HashSet<PhisicanSpecialization>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(2000)]
        public string PhotoLink { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VisitTimeLength { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientCalendar> PatientCalendar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanHealthCenter> PhisicanHealthCenter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanOpinion> PhisicanOpinion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanCalendarTemplate> PhisicanCalendarTemplate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanSpecialization> PhisicanSpecialization { get; set; }
    }
}
