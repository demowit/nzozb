namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhisicanHealthCenter")]
    public partial class PhisicanHealthCenter
    {
        public int Id { get; set; }

        public int? PhisicanId { get; set; }

        public int? HealthCenterId { get; set; }

        public virtual HealthCenter HealthCenter { get; set; }

        public virtual Phisican Phisican { get; set; }
    }
}
