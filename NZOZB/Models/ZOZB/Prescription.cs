namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Prescription")]
    public partial class Prescription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prescription()
        {
            MedicinePrescription = new HashSet<MedicinePrescription>();
            Prescription1 = new HashSet<Prescription>();
        }

        public int Id { get; set; }

        public int? PrescriptionNumber { get; set; }

        public int? PatientId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PrescriptionDate { get; set; }

        public int? Issued { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MedicinePrescription> MedicinePrescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Prescription> Prescription1 { get; set; }

        public virtual Prescription Prescription2 { get; set; }
    }
}
