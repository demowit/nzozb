namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhisicanSpecializationName")]
    public partial class PhisicanSpecializationName
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PhisicanSpecializationName()
        {
            PhisicanSpecialization = new HashSet<PhisicanSpecialization>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string SpecializationName { get; set; }

        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhisicanSpecialization> PhisicanSpecialization { get; set; }
    }
}
