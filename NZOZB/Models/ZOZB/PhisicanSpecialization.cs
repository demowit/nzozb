namespace NZOZB.Models.ZOZB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhisicanSpecialization")]
    public partial class PhisicanSpecialization
    {
        public int Id { get; set; }

        public int? PhisicanId { get; set; }

        public int? SpecializationNameId { get; set; }

        public byte? NotFinished { get; set; }

        public virtual Phisican Phisican { get; set; }

        public virtual PhisicanSpecializationName PhisicanSpecializationName { get; set; }
    }
}
