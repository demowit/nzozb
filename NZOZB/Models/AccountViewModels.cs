﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using NZOZB.Models.Common;
using NZOZB.Models.ZOZB;

namespace NZOZB.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Imię")]
        [StringLength(50, ErrorMessage = "Imię może mieć najwyżej 50 znaków")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Nazwisko")]
        [StringLength(100, ErrorMessage = "Nazwisko może mieć najwyżej 100 znaków")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Nazwisko panieńskie matki")]
        [StringLength(100, ErrorMessage = "Nazwisko panieńskie może mieć najwyżej 100 znaków")]
        public string MotherFamilyName { get; set; }
        [Required]
        [Display(Name = "Data urodzenia")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; } = new DateTime(1900, 1, 1);
        [Required]
        [Display(Name = "Pesel")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Pesel musi mieć 11 znaków")]
        public string Pesel { get; set; }
        [Required]
        [Display(Name = "Ulica")]
        [StringLength(100, ErrorMessage = "Nazwa ulicy musi mieć co najwyżej 100 znaków")]
        public string Street { get; set; }
        [Required]
        [Display(Name = "Miejscowość")]
        [StringLength(100, ErrorMessage = "Nazwa miejscowości musi mieć co najwyżej 100 znaków")]
        public string City { get; set; }
        [Required]
        [Display(Name = "Kod pocztowy")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "Kod pocztowy musi mieć dokładnie 6 znaków")]
        public string PostCode { get; set; }
        [Required]
        [Display(Name = "Numer telefonu")]
        [StringLength(12, MinimumLength = 9, ErrorMessage = "numer telefonu musi mieć co najmniej 9 cyfr i najwyżej 12")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Insurance card number")]
        public string InsuranceCardNumber { get; set; }
        [Required]
        [Display(Name = "Kod oddziału NFZ")]
        public int NFZCode { get; set; }
        [Required]
        [Display(Name = "Przychodnia")]
        public int HealthCenterId { get; set; }
        [Display(Name = "Lekarz")]
        public int PhisicanId { get; set; }
        public List<NFZRegionCode> NFZ {  get; set; }
        public List<ShortPhisicanInformation> Phisican { get; set; }
        public List<ShortCenterInformation> Center { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
