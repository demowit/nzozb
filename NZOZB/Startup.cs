﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NZOZB.Startup))]
namespace NZOZB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
